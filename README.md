# CashID Identity Manager

A basic identity manager for android based on CashID / BitID.

# Dependencies

# Build setup instructions

git clone https://gitlab.com/cashid/identity-manager.git

cd identity-manager/

npm install

cordova plugin add src/plugins/color_parser/

cordova plugin add src/plugins/certificate_parser/

cordova plugin add src/plugins/profile_parser/

cordova platform add android

browserify www/js/source.js -o www/js/application.js

cordova run android