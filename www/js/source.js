// Enable use of moment to handle time.
const moment = require('moment');

// Enable jQuery and Bootstrap support.
var $ = require('jquery');
window.$ = $;
require('bootstrap');

const defaultSettings =
{
	confirmSuccess: true,
	enableAnonymous: false,
	shareInformation: true,
	allowRemoval: false,
	requestContactPermission: false 
};

const defaultMetadata =
{
	profile: {},
	sharing: {} 
};

// Regular expressions from the specification to simplify parsing.
const regnames = 
{
	request:
	{
		scheme: 1,
		domain: 2,
		path: 3,
		parameters: 4
	},
	parameters:
	{
		action: 1,
		data: 2,
		required: 3,
		optional: 4,
		nonce: 5
	},
	metadata:
	{
		identification: 1,
		name: 2,
		family: 3,
		nickname: 4,
		age: 5,
		gender: 6,
		birthdate: 7,
		picture: 8,
		national: 9,
		position: 10,
		country: 11,
		state: 12,
		city: 13,
		street: 14,
		residence: 15,
		coordinate: 16,
		contact: 17,
		email: 18,
		instant: 19,
		social: 20,
		phone: 21,
		postal: 22
	}
}
const regexps =
{
	'request': /(cashid:)(?:[\/]{2})?([^\/]+)(\/[^\?]+)(\?.+)/,
	'parameters': /(?:(?:[\?\&]{1}a=)([^\&]+))?(?:(?:[\?\&]{1}d=)([^\&]+))?(?:(?:[\?\&]{1}r=)([^\&]+))?(?:(?:[\?\&]{1}o=))?([^\&]+)?(?:[\?\&]{1}x=)([^\&]+)?/,
	'metadata': /(?:i((?![1-9]+))?(1)?(2)?(3)?(4)?(5)?(6)?(8)?(9)?)?(?:p((?![1-9]+))?(1)?(2)?(3)?(4)?(6)?(9)?)?(?:c((?![1-9]+))?(1)?(2)?(3)?(4)?(5)?)?/	
}

const VIBRATE =
{
	// Single quick tap.
	NOTIFY: [125],

	// Knock, Knock, Knock
	ATTENTION: [125, 100, 125, 100, 125],

	// Short and long, order depending on outcome.
	SUCCESS: [200, 66, 500],
	ERROR: [500, 66, 200],
}

// Enable alert based debugging.
window.onerror = function(message, url, lineNo)
{
	alert('Error: ' + message + '\n' + 'Line Number: ' + lineNo);
	return true;
}

// ???
function CashID()
{
	// Set the servicePrefix.
	this.servicePrefix = 'service_';

	// Create a local instance of the bitcoincashjs library.
	this.bitcoin = require('bitcore-lib-cash');
	this.bitcoin.message = require('bitcore-message');

	// Configure default settings, if necessary.
	if(typeof localStorage.settings === 'undefined')
	{
		localStorage.settings = JSON.stringify(defaultSettings);
	}

	// Configure default settings, if necessary.
	if(typeof localStorage.metadata === 'undefined')
	{
		localStorage.metadata = JSON.stringify(defaultMetadata);
	}

	// Create an empty service list, if necessary
	if(typeof localStorage.services === 'undefined')
	{
		localStorage.services = JSON.stringify({});
	}

	// Create an empty identity list, if necessary
	if(typeof localStorage.identities === 'undefined')
	{
		localStorage.identities = JSON.stringify({});
	}

	// Create an empty identity template.

	// When answering scope requests null, false and value have special meanings:
	// null: indicates that this property has not been requested before
	// false: indicates that this property should never be submitted
	// value: indicates that this property should always be submitted with this value
	this.templates =
	{
		identity:
		{
			privateKey: null,
			publicAddress: null,
			created: null,
			metadata: {},
			permissions: {}
		}
	};

	this.prepare = function()
	{
		// Finalize startup when the device is ready.
		document.addEventListener('deviceready', window.cashid.startup, false);

		// Set opacity to 0 to make it become transparent over 1.00s.
		document.getElementById('loading').style.opacity = 0.5;

		// Apply the application settings.
		window.cashid.parseSettings();

		// Apply the application settings.
		window.cashid.parseMetadata();

		// Update and show all existing services.
		window.cashid.showServiceList();

		// Keep updating the service list every 5 seconds.
		setInterval(function() { window.cashid.showServiceList(); }, 5000);
	};

	this.startup = function()
	{
		// Set opacity to 0 to make it become transparent over time.
		document.getElementById('loading').style.opacity = 0;

		// Remove the blocking element in 0.50s.
		setTimeout(function(){ document.getElementById('loading').style.display = 'none'; }, 500);

		// Load the existing settings.
		let currentSettings = JSON.parse(localStorage.settings);

		//
		if(!currentSettings.requestContactPermission)
		{
			// Show the permission request information.
			$('#permission_contact').modal('show');

			// Set the first-use flag.
			currentSettings.requestContactPermission = true;
		}

		// Store the settings in local storage
		localStorage.settings = JSON.stringify(currentSettings);

		// Prevent back button from exiting the application.
		document.addEventListener("backbutton", window.cashid.hideModals, false);

		// Enable the application to pause and resume.
		document.addEventListener("pause", window.cashid.pause, false);
		document.addEventListener("resume", window.cashid.resume, false);

		// Prepare the QR scanner for faster deployment, if previously authorized.
		QRScanner.getStatus(function(status) { if(status.authorized) { QRScanner.prepare(); } });
	};

	this.pause = function()
	{
		
	}

	this.resume = function()
	{
		
	}

	this.hideModals = function(event)
	{
		// Hide the verify identity dialog.
		$('#permission_contact').modal('hide');
		$('#share_metadata').modal('hide');
		$('#verify_identity').modal('hide');
		$('#configuration').modal('hide');
		$('#metadata').modal('hide');

		//
		//event.preventDefault();
	}

	this.resetContent = function()
	{
		// Reset all settings and data.
		localStorage.settings = JSON.stringify(defaultSettings);
		localStorage.metadata = JSON.stringify(defaultMetadata);
		localStorage.services = JSON.stringify({});
		localStorage.identities = JSON.stringify({});

		// Reload the application.
		location.reload(); 
	}

	this.updateSettings = function(setting, value)
	{
		// Load the existing settings.
		let currentSettings = JSON.parse(localStorage.settings);

		// Apply the new value.
		currentSettings[setting] = value;

		// Store the settings in local storage
		localStorage.settings = JSON.stringify(currentSettings);

		// Update the results of the new value.
		this.parseSettings();
	}

	this.parseSettings = function()
	{
		// Load the existing settings.
		let currentSettings = JSON.parse(localStorage.settings);

		// Show/Hide the anonymous entry.
		document.getElementById('anonymous_identity').style.display = (currentSettings.enableAnonymous ? 'block' : 'none');

		// Check/Uncheck the settings.
		document.getElementById('input_enable_anonymous').checked = (currentSettings.enableAnonymous ? true : false);
		document.getElementById('input_share_information').checked = (currentSettings.shareInformation ? true : false);
		document.getElementById('input_confirm_success').checked = (currentSettings.confirmSuccess ? true : false);
		document.getElementById('input_allow_removal').checked = (currentSettings.allowRemoval ? true : false);
	}

	this.createIdentity = function(identityData, permissionData)
	{
		// Create a new private key and corresponding address.
		let privateKey = new this.bitcoin.PrivateKey();
		let publicAddress = privateKey.toAddress().toString(this.bitcoin.Address.CashAddrFormat).substring(12);
		//let publicAddress = privateKey.toAddress().toString();

		// Load the existing identity list.
		let currentIdentityList = JSON.parse(localStorage.identities);
		
		// initialize an empty identity template.
		currentIdentityList[publicAddress] = this.templates['identity'];

		// Update the address and creation date.
		currentIdentityList[publicAddress].privateKey = privateKey.toWIF();
		currentIdentityList[publicAddress].publicAddress = publicAddress;
		currentIdentityList[publicAddress].created = moment();

		// Store the identities back into local storage.
		localStorage.identities = JSON.stringify(currentIdentityList);

		// populate the identity with identification data.
		this.updateIdentity(publicAddress, identityData, permissionData);

		// Return the address identifier of this identity.
		return publicAddress;
	}

	this.updateIdentity = function(identityAddress, identityData, permissionData)
	{
		// Load the existing identity list.
		let currentIdentityList = JSON.parse(localStorage.identities);

		// Clone the identity from local storage.
		let currentIdentity = currentIdentityList[identityAddress];

		// Merge the old metadata and permissions with the new.
		Object.assign(currentIdentity.metadata, identityData);
		Object.assign(currentIdentity.permissions, permissionData);

		// Merge the identity with the identity list.
		currentIdentityList[identityAddress] = currentIdentity;

		// Store the updated identity list back to local storage.
		localStorage.identities = JSON.stringify(currentIdentityList);
	}

	//
	this.deleteIdentity = function(identityAddress)
	{
		// TODO: Remove the identity from local storage.
		alert('not done yet');
	}

	//
	this.authenticateFromIdentificationDialog = function(identity = null, markForRemoval = false)
	{
		//
		let currentServiceList = JSON.parse(localStorage.services);

		// Get the temporary service data from a JSON on the dialog.
		let currentService = JSON.parse(document.getElementById('verify_identity').getAttribute('data-json'));

		// Remove unauthorized metadata.
		for(let key in window.cashid.metadata.control)
		{
			if(window.cashid.metadata.control[key] === false)
			{
				delete window.cashid.metadata.fields[key];
			}
		}

		if(identity === false)
		{
			// Create a new keypair.
			let privateKey = new this.bitcoin.PrivateKey();

			// Authenticate with the new keypair and the temporary metadata.
			this.authenticateToService(privateKey.toWIF(), window.cashid.metadata.fields);
		}
		else
		{
			if(identity === null)
			{
				// Create a new empty identity.
				identity = this.createIdentity(window.cashid.metadata.fields, window.cashid.metadata.control);
			}
			else
			{
				// Update the metadata for this identity.
				this.updateIdentity(identity, window.cashid.metadata.fields, window.cashid.metadata.control)
			}

			// Create a new service, if necessary
			if(typeof currentServiceList[currentService.D] !== "object")
			{
				this.createService(currentService);
			}

			// Link the identity to the service.
			this.linkIdentityToService(currentService.D, identity);

			// Load the existing identity list.
			let currentIdentityList = JSON.parse(localStorage.identities);

			// Authenticate to the service with the new identity.
			this.authenticateToService(currentIdentityList[identity].privateKey, window.cashid.metadata.fields, markForRemoval);

			// Update the displayed service list. 
			this.showServiceList();
		}

		// Delete remaining service information from the DOM
		document.getElementById('verify_identity').removeAttribute('data-json')
		
		// Hide the verify identity dialog.
		$('#share_metadata').modal('hide');
	}
	
	this.authenticateToService = function(identityWIF, identityMetadata, markForRemoval = false)
	{
		let privateKey = new this.bitcoin.PrivateKey(identityWIF);
		let publicAddress = privateKey.toAddress().toString(this.bitcoin.Address.CashAddrFormat).substring(12);
		//let publicAddress = privateKey.toAddress().toString();
		let signature = this.bitcoin.message(this.serviceData.R).sign(privateKey);

		let requestProtocolSwapper = new RegExp('cashid:[\/]{0,2}');
		let requestUri = this.serviceData.R.substring(0, this.serviceData.R.indexOf('?')).replace(requestProtocolSwapper, 'https://');
		let requestData = JSON.stringify({ request: this.serviceData.R, address: publicAddress, signature: signature, metadata: identityMetadata });

		// Store the identity address in the service data temporarily.
		this.serviceData.A = publicAddress;

		let xmlhttp = new XMLHttpRequest();

		xmlhttp.open("POST", requestUri, true);

		// If this identity should be removed...
		if(markForRemoval)
		{
			xmlhttp.addEventListener("load", this.confirmAuthenticationWithRemoval);
			xmlhttp.addEventListener("error", this.failedAuthentication);
		}
		// If this identity should be kept..
		else
		{
			xmlhttp.addEventListener("load", this.confirmAuthentication);
			xmlhttp.addEventListener("error", this.failedAuthentication);
		}

		xmlhttp.setRequestHeader("Content-Type", "application/json");
		xmlhttp.send(requestData);
	}

	this.authenticateWithExistingIdentity = function(publicAddress, markForRemoval = false)
	{
		// Hide the verify identification dialog.
		$('#verify_identity').modal('hide');

		// Update the metadata dialog based on existing identity.
		let newInformation = this.updateMetadataDialog(publicAddress);

		// Load the existing settings.
		let currentSettings = JSON.parse(localStorage.settings);

		// If sharing of information is enabled...
		if(currentSettings.shareInformation)
		{
			// If there is new information, show the user their options.
			if(newInformation)
			{
				// show the verify identification dialog.
				$('#share_metadata').modal('show');
			}
			else
			{
				// If there was no new information, perform the authentication.
				window.cashid.authenticateFromIdentificationDialog(window.cashid.metadata.identity, markForRemoval);
			}
		}
		else
		{
			// perform authentication.
			window.cashid.authenticateFromIdentificationDialog(window.cashid.metadata.identity, markForRemoval);
		}
	}

	//
	this.authenticateWithNewIdentity = function()
	{
		// Hide the verify identification dialog.
		$('#verify_identity').modal('hide');

		// Update the metadata dialog based on default profile.
		let newInformation = this.updateMetadataDialog(null);

		// Load the existing settings.
		let currentSettings = JSON.parse(localStorage.settings);

		// If sharing of information is enabled...
		if(currentSettings.shareInformation && newInformation)
		{
			// show the verify identification dialog.
			$('#share_metadata').modal('show');
		}
		else
		{
			// perform authentication.
			window.cashid.authenticateFromIdentificationDialog(window.cashid.metadata.identity);
		}
	}

	//
	this.authenticateWithAnonymous = function()
	{
		// Hide the verify identification dialog.
		$('#verify_identity').modal('hide');

		// Update the metadata dialog based on anonymous identity.
		this.updateMetadataDialog(false);

		// Load the existing settings.
		let currentSettings = JSON.parse(localStorage.settings);

		// If sharing of information is enabled...
		if(currentSettings.shareInformation)
		{
			// show the verify identification dialog.
			$('#share_metadata').modal('show');
		}
		else
		{
			// perform authentication.
			window.cashid.authenticateFromIdentificationDialog(window.cashid.metadata.identity);
		}
	}

	this.failedAuthentication = function(event)
	{
		alert('Failed to contact service provider. Error: ' + this.responseText);
	}

	// If we should remove this identity..
	this.confirmAuthenticationWithRemoval = function(event)
	{
		// Call the confirmation with removal set to true.
		window.cashid.confirmAuthentication(this, true);
	}
	
	this.confirmAuthentication = function(event, markForRemoval = false)
	{
		// Parse the confirmation.
		let confirmation = JSON.parse((this.responseText ? this.responseText : event.responseText));

		if(confirmation.status > 0)
		{
			// Vibrate.
			navigator.vibrate(VIBRATE['ERROR']);
		}
		else
		{
			// Vibrate.
			navigator.vibrate(VIBRATE['SUCCESS']);
		}

		// Load the existing services list.
		let currentServiceList = JSON.parse(localStorage.services);

		// If this is a stored identity..
		if(typeof currentServiceList[window.cashid.serviceData.D] !== 'undefined' && typeof currentServiceList[window.cashid.serviceData.D].identities[window.cashid.serviceData.A] === 'object')
		{
			// ..update the last used timestamp.
			currentServiceList[window.cashid.serviceData.D].identities[window.cashid.serviceData.A].last_used = moment();
		}

		// Store the updated services list back in local storage.
		localStorage.services = JSON.stringify(currentServiceList);

		// Delete remaining service information from the DOM
		document.getElementById('verify_identity').removeAttribute('data-json')

		// Hide the verify identification dialog.
		$('#verify_identity').modal('hide');

		// Update the display of existing services, to refresh the last used text.
		window.cashid.showServiceList();

		if(confirmation.status > 0)
		{
			//
			alert('Error: ' + confirmation.message);

			//
			if(markForRemoval)
			{
				alert('Not removing identity just yet...');
			}
		}
		else
		{
			// Load the existing settings.
			let currentSettings = JSON.parse(localStorage.settings);

			// If sharing of information is enabled...
			if(currentSettings.confirmSuccess)
			{
				// TODO: Check if we should show successfull responses.
				alert('Authentication successfull.');
			}

			// If this identity should be removed...
			if(markForRemoval)
			{
				alert('Not removing identity, function not yet implemented in this application.');
			}
		}
	}
	
	// 
	this.createService = function(serviceData)
	{
		//  Load the existing services list.
		let currentServiceList = JSON.parse(localStorage.services);

		// Initialize the service with the data provided and an empty list of identities.
		currentServiceList[serviceData.D] = serviceData;
		currentServiceList[serviceData.D].identities = {};

		// Store the updated list of services back to local storage.
		localStorage.services = JSON.stringify(currentServiceList);
	}

	// 
	this.linkIdentityToService = function(serviceDomain, identityAddress)
	{
		// Load the existing services list.
		let currentServiceList = JSON.parse(localStorage.services);

		// Check that the service domain exist in the list of service..
		if(typeof currentServiceList[serviceDomain] !== "object")
		{
			// TODO: Add proper debug messages here.
			alert('Trying to link identity to nonexisting domain: ' + serviceDomain);
		}
		else
		{
			// Link the provided identity to the service.
			currentServiceList[serviceDomain].identities[identityAddress] = { address: identityAddress, first_used: moment(), last_used: moment() };

			// Store the updated services list back in local storage.
			localStorage.services = JSON.stringify(currentServiceList);
		}
	}

	// Update the metadata dialog based on a provided identity, default user profile or none at all.
	this.updateMetadataDialog = function(metadataIdentity = null)
	{
		let requestParts = regexps.request.exec(window.cashid.serviceData.R);
		let requestParameters = regexps.parameters.exec(requestParts[regnames.request.parameters]);

		// Remove all previous form elements.
		{
			let shareNode = document.getElementById("share_metadata");
			let inputNodes = shareNode.getElementsByClassName("form-check");

			while(inputNodes[0])
			{
				inputNodes[0].parentNode.removeChild(inputNodes[0]);
			}
		}

		// Reset / Initialize an empty structure for managing temporary metadata.
		window.cashid.metadata =
		{
			identity: metadataIdentity,
			disabled: {},
			control: {},
			fields: {}
		}

		// Load the existing settings.
		let currentSettings = JSON.parse(localStorage.settings);

		//
		if(currentSettings.shareInformation)
		{
			// Set a boolean storing if new information was requested.
			let newInformation = false;

			if(!requestParameters[regnames.parameters.required])
			{
				// Add the field to the form.
				document.getElementById("metadata_required").innerHTML = "<h5>Required information</h5><small class='form-text text-muted'>The service did not ask for required information.</small>"
			}

			if(!requestParameters[regnames.parameters.optional])
			{
				// Add the field to the form.
				document.getElementById("metadata_optional").innerHTML = "<h5>Optional information</h5><small class='form-text text-muted'>The service did not ask for optional information.</small>"
			}

			//
			if(requestParameters[regnames.parameters.required] || requestParameters[regnames.parameters.optional])
			{
				let requiredCodes = regexps.metadata.exec(requestParameters[regnames.parameters.required]);
				let optionalCodes = regexps.metadata.exec(requestParameters[regnames.parameters.optional]);

				let requiredFields = {};
				let optionalFields = {};

				//
				for(let fieldName in regnames.metadata)
				{
					requiredFields[fieldName] = requiredCodes[regnames.metadata[fieldName]];
					optionalFields[fieldName] = optionalCodes[regnames.metadata[fieldName]];
				}

				for(let fieldName in requiredFields)
				{
					if(requiredFields[fieldName])
					{
						let metadataSource = document.getElementById("metadata_" + fieldName);
						if(metadataSource == null)
						{
							alert("Required metadata field is not supported: " + fieldName);
						}
						else
						{
							let metadataNode = metadataSource.cloneNode(true);

							if(typeof metadataNode !== 'undefined')
							{
								// If we should use a blank empty identity.
								if(metadataIdentity === false)
								{
									// Initialize this metadata in the temporary metadata structure.
									window.cashid.metadata.disabled[fieldName] = false;
									window.cashid.metadata.control[fieldName] = true;
									window.cashid.metadata.fields[fieldName] = null;
								}
								// If we should use the default identity.
								else if(metadataIdentity === null)
								{
									// Initialize this metadata in the temporary metadata structure.
									window.cashid.metadata.disabled[fieldName] = false;
									window.cashid.metadata.control[fieldName] = true;
									window.cashid.metadata.fields[fieldName] = metadataNode.getElementsByClassName("metadata_value")[0].value;

									// Set new information to true as we have never shared with this identity before.
									newInformation = true;
								}
								else
								{
									// Load the existing identity list.
									let currentIdentityList = JSON.parse(localStorage.identities);

									// Clone the identity from local storage.
									let currentIdentity = currentIdentityList[metadataIdentity];

									//
									let currentValue = ((typeof currentIdentity.metadata[fieldName] !== 'undefined') ? currentIdentity.metadata[fieldName] : false)

									// Initialize this metadata in the temporary metadata structure.
									window.cashid.metadata.disabled[fieldName] = (currentValue ? true : false);
									window.cashid.metadata.control[fieldName] = true;
									window.cashid.metadata.fields[fieldName] = (currentValue ? currentValue : metadataNode.getElementsByClassName("metadata_value")[0].value);

									// If this field has not been previously supplied.
									if(!currentValue)
									{
										newInformation = true;
									}
								}

								// Replace the duplicated ID.
								metadataNode.id = "share_metadata_" + fieldName;

								// Remove on-click.
								metadataNode.getElementsByClassName("metadata_control")[0].onchange = function() { this.checked = true; };
								metadataNode.getElementsByClassName("metadata_control")[0].checked = true;
								metadataNode.getElementsByClassName("metadata_control")[0].disabled = window.cashid.metadata.disabled[fieldName];

								// Add a handler to update changes by the user.
								metadataNode.getElementsByClassName("metadata_value")[0].disabled = window.cashid.metadata.disabled[fieldName];
								metadataNode.getElementsByClassName("metadata_value")[0].onchange = function() { window.cashid.metadata.fields[fieldName] = this.value; };
								metadataNode.getElementsByClassName("metadata_value")[0].value = window.cashid.metadata.fields[fieldName];

								// Add the field to the form.
								document.getElementById("metadata_required").appendChild(metadataNode);
							}
						}
					}
				}

				for(let fieldName in optionalFields)
				{
					if(optionalFields[fieldName])
					{
						let metadataSource = document.getElementById("metadata_" + fieldName);
						if(metadataSource == null)
						{
							alert("Optional metadata field is not supported: " + fieldName);
						}
						else
						{
							let metadataNode = metadataSource.cloneNode(true);

							if(typeof metadataNode !== 'undefined')
							{
								// If we should use a blank empty identity.
								if(metadataIdentity === false)
								{
									// Initialize this metadata in the temporary metadata structure.
									window.cashid.metadata.disabled[fieldName] = false;
									window.cashid.metadata.control[fieldName] = false;
									window.cashid.metadata.fields[fieldName] = null;
								}
								// If we should use the default identity.
								else if(metadataIdentity === null)
								{
									// Initialize this metadata in the temporary metadata structure.
									window.cashid.metadata.disabled[fieldName] = false
									window.cashid.metadata.control[fieldName] = metadataNode.getElementsByClassName("metadata_control")[0].checked;
									window.cashid.metadata.fields[fieldName] = metadataNode.getElementsByClassName("metadata_value")[0].value;

									// Set new information to true as we have never shared with this identity before.
									newInformation = true;
								}
								else
								{
									// Load the existing identity list.
									let currentIdentityList = JSON.parse(localStorage.identities);

									// Clone the identity from local storage.
									let currentIdentity = currentIdentityList[metadataIdentity];

									//
									let currentValue = ((typeof currentIdentity.metadata[fieldName] !== 'undefined') ? currentIdentity.metadata[fieldName] : false)

									// Initialize this metadata in the temporary metadata structure.
									window.cashid.metadata.disabled[fieldName] = (currentValue ? true : false);
									window.cashid.metadata.control[fieldName] = (currentValue ? true : false);
									window.cashid.metadata.fields[fieldName] = (currentValue ? currentValue : metadataNode.getElementsByClassName("metadata_value")[0].value);

									// If this field has not been previously supplied.
									if(!currentValue)
									{
										newInformation = true;
									}
								}

								// Replace the duplicated ID.
								metadataNode.id = "share_metadata_" + fieldName;

								// Remove on-click.
								metadataNode.getElementsByClassName("metadata_control")[0].onchange = function() { window.cashid.metadata.control[fieldName] = this.checked; };
								metadataNode.getElementsByClassName("metadata_control")[0].checked = window.cashid.metadata.control[fieldName];
								metadataNode.getElementsByClassName("metadata_control")[0].disabled = window.cashid.metadata.disabled[fieldName];

								// Add a handler to update changes by the user, if allowed.
								metadataNode.getElementsByClassName("metadata_value")[0].disabled = window.cashid.metadata.disabled[fieldName];
								metadataNode.getElementsByClassName("metadata_value")[0].onchange = function() { window.cashid.metadata.fields[fieldName] = this.value; };
								metadataNode.getElementsByClassName("metadata_value")[0].value = window.cashid.metadata.fields[fieldName];

								// Add the field to the form.
								document.getElementById("metadata_optional").appendChild(metadataNode);
							}
						}
					}
				}
			}

			// Inform the caller if any information is new.
			return newInformation;
		}
		else
		{
			// Inform the caller that no information is new.
			return false;
		}
	}
	
	//
	this.updateRequestDialog = function()
	{
		let requestParts = regexps.request.exec(window.cashid.serviceData.R);
		let requestParameters = regexps.parameters.exec(requestParts[regnames.request.parameters]);

		let actionTitles =
		{
			'auth': "Authenticate to",
			'login': "Login to",
			'sign': "Sign for",
			'register': "Register for <b>" + requestParameters[regnames.parameters.data] + "</b><br/> provided by",
			'ticket': "Receive ticket from",
			'claimtx': "Show receipt to",
			'claimaddr': "Prove ownership to",
			'delete': "Request account removal at",
			'logout': "Request session closure at",
			'revoke': "Submit identity revocation to",
			'update': "Submit profile changes to"
		};

		let actionNames =
		{
			'auth': "authentication",
			'login': "login",
			'sign': "signing",
			'register': "registration",
			'ticket': "ticker offer",
			'claimtx': "receipt proval",
			'claimaddr': "address proval",
			'delete': "removal",
			'logout': "logout",
			'revoke': "revocation",
			'update': "update"
		};

		//
		let enableNewIdentities = (requestParameters[regnames.parameters.action] != 'delete' ? true : false);
		let markForRemoval = (requestParameters[regnames.parameters.action] == 'delete' ? true : false);

		// Set default action.
		let title = actionTitles['auth'];
		let name = actionNames['auth'];
		
		// If the request has a listed action..
		if(requestParameters[regnames.parameters.action])
		{
			// Grab a title for the action
			title = actionTitles[requestParameters[regnames.parameters.action]];
			name = actionNames[requestParameters[regnames.parameters.action]];
		}

		// Set the O as provider if possible, otherwise use CN.
		let providerName = (window.cashid.serviceData.O ? window.cashid.serviceData.O : window.cashid.serviceData.CN);

		// Update the text color.
		document.getElementById('verify_identity').getElementsByClassName('modal-header')[0].style.backgroundColor = window.cashid.serviceData.S['background-color'];
		document.getElementById('share_metadata').getElementsByClassName('modal-header')[0].style.backgroundColor = window.cashid.serviceData.S['background-color'];

		// Update the background color.
		document.getElementById('verify_identity').getElementsByClassName('modal-title')[0].style.color = window.cashid.serviceData.S['color'];
		document.getElementById('share_metadata').getElementsByClassName('modal-title')[0].style.color = window.cashid.serviceData.S['color'];

		// Update the service name.
		document.getElementById('request_provider').innerHTML = providerName;
		document.getElementById('request_provider_metadata').innerHTML = providerName;

		// Update the service name.
		document.getElementById('request_title').innerHTML = title;

		// Update the request type.
		document.getElementById('request_type').innerHTML = name;
		document.getElementById('request_type_metadata').innerHTML = name;

		// Load the existing settings.
		let currentSettings = JSON.parse(localStorage.settings);

		/*********/

		// Load the list of existing services.
		let currentServiceList = JSON.parse(localStorage.services);

		if(currentServiceList[window.cashid.serviceData.D] && currentServiceList[window.cashid.serviceData.D].defaultIdentity)
		{
			// Store the temporary service data as a JSON on the dialog.
			document.getElementById('verify_identity').setAttribute('data-json', JSON.stringify(window.cashid.serviceData));

			return window.cashid.authenticateWithExistingIdentity(currentServiceList[window.cashid.serviceData.D].defaultIdentity, false);
		}

		//
		if(!currentSettings.shareInformation)
		{
			if(requestParameters[regnames.parameters.required])
			{
				alert('Request has required metadata, but metadata sharing is disabled.');
			}
			else
			{
				// Show the service to the user interface.
				window.cashid.showIdentifyToServiceDialog(enableNewIdentities, markForRemoval);
			}
		}
		else
		{
			// Show the service to the user interface.
			window.cashid.showIdentifyToServiceDialog(enableNewIdentities, markForRemoval);
		}
	}

	//
	this.addServiceColors = function(colors)
	{
		// Store the parsed colors.
		window.cashid.serviceData.S = JSON.parse(colors);

		// Update the request dialog.
		window.cashid.updateRequestDialog();
	}

	//
	this.storeServiceFavicon = function(result)
	{
		//alert('storeServiceFavicon');
		// Parse the colors from the avatar.
		cordova.plugins.color_parser.getColorStyleFromImageFile(window.cashid.serviceData.F, window.cashid.addServiceColors, alert);
	}

	//
	this.ignoreServiceFavicon = function(result)
	{
		//alert('ignoreServiceFavicon');
		window.cashid.addServiceColors("{ 'color': '#333333', 'background-color': '#dddddd' }");
	}

	this.downloadServiceFavicon = function()
	{
		let fileTransfer = new FileTransfer();
		let filename = cordova.file.dataDirectory + "favicons/" + window.cashid.serviceData.D + ".ico";

		if(window.cashid.serviceData.D != 'yours.org')
		{
			var uri = encodeURI("https://" + window.cashid.serviceData.D + "/favicon.ico");
		}
		else
		{
			var uri = encodeURI("https://" + window.cashid.serviceData.D + "/static/img/favicon-green.ico");
		}

		//alert('downloadServiceFavicon');
		fileTransfer.download(uri, filename, this.storeServiceFavicon, this.ignoreServiceFavicon);
	}

	this.addServiceFavicon = function(serviceDomain)
	{
		let filename = cordova.file.dataDirectory + "favicons/" + serviceDomain + ".ico";

		// Store the filename without protocol.
		this.serviceData.F = filename.substr(7);

		window.resolveLocalFileSystemURL(filename, this.storeServiceFavicon, this.downloadServiceFavicon);
	}

	this.parseServiceCertificate = function(subject)
	{
		// Here comes one of my ugliest hacks ever, but I couldn't find a rfc2253 parsing library for javascript.
		//alert(subject);

		// Step 1: replace all backslashncoded commas with a really unique string.
		subject = subject.replace(/\\\,/g, "®þħ");

		// Step 2: regexp match the various parts
		let patterns = 
		{
			O: /O=([^,]+)/,	// Organization
			CN: /CN=([^,]+)/,	// Common name
			C: /C=([^,]+)/,	// Country
			ST: /ST=([^,]+)/,	// State
			L: /L=([^,]+)/		// City
		};

		for(pattern in patterns)
		{
			let match = patterns[pattern].exec(subject);

			if(match !== null)
			{
				// Revert the ugly unique string back into a regular comma.
				window.cashid.serviceData[pattern] = match[1].replace("®þħ", ",");
				
				//alert(pattern + "=" + this.serviceData[pattern]);
			}
		}

		// Step 2: Load the favicon.
		window.cashid.addServiceFavicon(window.cashid.serviceData.D);
	}

	// 
	this.identifyToService = function(serviceDomain, request)
	{
		// Initialize an empty serviceData structure.
		this.serviceData = 
		{
			D: serviceDomain,
			A: null,
			O: null,
			CN: null,
			C: null,
			ST: null,
			L: null,
			F: null,
			S: null,
			R: request
		};

		//alert('identifyToService');
		// Step 1: Load the certificate.
		cordova.plugins.certificate_parser.fetchX509SubjectFromDomain('https://' + serviceDomain, this.parseServiceCertificate, alert);
	}

	this.showIdentifyToServiceDialog = function(enableCreateNewIdentities = true, markForRemoval = false)
	{
		// Store the temporary service data as a JSON on the dialog.
		document.getElementById('verify_identity').setAttribute('data-json', JSON.stringify(window.cashid.serviceData));

		// TODO: Update with list of existing identities...

		// Load the list of existing services and identities.
		let currentServiceList = JSON.parse(localStorage.services);
		let currentIdentityList = JSON.parse(localStorage.identities);

		let last_used_entry = document.getElementById('last_used_identity');
		let linked_identities_entry = document.getElementById('linked_identities');

		// Check if this service has never been been used before..
		if(typeof currentServiceList[window.cashid.serviceData.D] !== "object" || Object.keys(currentServiceList[window.cashid.serviceData.D].identities).length < 1)
		{
			document.getElementById('recently_used_section').style.display = 'none';

			linked_identities_entry.innerHTML = "<li>No identities exist for this service</li>";
			linked_identities_entry.style.opacity = 0.5;
			linked_identities_entry.style.textAlign = "center";
		}
		else
		{
			document.getElementById('recently_used_section').style.display = 'block';

			linked_identities_entry.innerHTML = "";
			linked_identities_entry.style.opacity = 1;
			linked_identities_entry.style.textAlign = "left";

			// initialize and empty last used identity.
			let lastUsedIdentity = null;

			for(let currentIdentity in currentServiceList[window.cashid.serviceData.D].identities)
			{
				linked_identities_entry.innerHTML += "<li onclick=\"window.cashid.authenticateWithExistingIdentity('" + currentIdentity + "', " + (markForRemoval ? 'true' : 'false') + ");\"><span class='identity_logo far fa-user-circle'></span><span class='identity_name'>" + this.getIdentityTitle(currentIdentity) + "</span></li>";

				// Check if the the current identity was used later than the current best candidate for last used..
				if(lastUsedIdentity === null || moment(currentServiceList[window.cashid.serviceData.D].identities[lastUsedIdentity].last_used).isBefore(currentServiceList[window.cashid.serviceData.D].identities[currentIdentity].last_used))
				{
					// .. and if so, set the current to the new candidate.
					lastUsedIdentity = currentIdentity;
				}
			}

			last_used_entry.innerHTML = "<li onclick=\"window.cashid.authenticateWithExistingIdentity('" + lastUsedIdentity + "', " + (markForRemoval ? 'true' : 'false') + ");\"><span class='identity_logo far fa-clock'></span><span class='identity_name'>" + this.getIdentityTitle(lastUsedIdentity) + "</span></li>";
		}

		// If this request makes sense to non-existing accounts..
		if(enableCreateNewIdentities)
		{
			// Show the options that create new identities.
			document.getElementById('create_new_section').style.display = 'block';
		}
		else
		{
			// Hide the options that create new identities.
			document.getElementById('create_new_section').style.display = 'none';
		}

		// Show the verify identity dialog.
		$('#verify_identity').modal('show');
	}

	// Removes a service from local storage.
	this.delService = function(serviceDomain)
	{
		// Hide the service to the user interface.
		hideService(serviceDomain);
	}

	this.hideServiceList = function()
	{
		// Remove all previous form elements.
		{
			let serviceListNode = document.getElementById('service_list');
			let serviceTemplateNode = document.getElementById(this.servicePrefix + 'template').cloneNode(true);

			serviceListNode.innerHTML = "";
			serviceListNode.appendChild(serviceTemplateNode);
		}
	}

	this.toggleDefaultIdentity = function()
	{
		//
		let currentServiceDomain = this.parentNode.parentNode.getAttribute('data-service-domain');

		// Get the list of available services..
		let currentServiceList = JSON.parse(localStorage.services);

		// If the current service is the automatically selected one..
		if(currentServiceList[currentServiceDomain].defaultIdentity == this.getAttribute('data-identity'))
		{
			// remove it so none is selected.
			delete currentServiceList[currentServiceDomain].defaultIdentity;
		}
		else
		{
			currentServiceList[currentServiceDomain].defaultIdentity = this.getAttribute('data-identity');
		}

		// Store the updated services list back in local storage.
		localStorage.services = JSON.stringify(currentServiceList);

		// Update the service list.
		window.cashid.showService(currentServiceDomain);
	}

	this.showServiceList = function()
	{
		// Get the list of available services..
		let currentServiceList = JSON.parse(localStorage.services);

		// For each service..
		for(currentService in currentServiceList)
		{
			// Show the service on the display.
			this.showService(currentService);
		}
	}

	this.getIdentityTitle = function(Identity)
	{
		let currentIdentityList = JSON.parse(localStorage.identities);
		let currentMetadata = currentIdentityList[Identity].metadata;

		if(typeof currentMetadata.name != 'undefined' && typeof currentMetadata.family != 'undefined')
		{
			return currentMetadata.name + " " + currentMetadata.family;
		}

		if(typeof currentMetadata.nickname != 'undefined')
		{
			return currentMetadata.nickname;
		}

		if(typeof currentMetadata.email != 'undefined')
		{
			return currentMetadata.email;
		}

		return Identity;
	}

	// Display or updates a service in the user interface.
	this.showService = function(serviceDomain)
	{
		// If the service is already shown..
		if(document.getElementById(this.servicePrefix + serviceDomain))
		{
			// Remove it so we can get it updated...
			// FIXME: make this more sane please.
			let tempNode = document.getElementById(this.servicePrefix + serviceDomain);
			tempNode.parentNode.removeChild(tempNode);
		}

		// Lookup the service template node.
		let serviceTemplateNode = document.getElementById(this.servicePrefix + 'template');

		// Create a copy of the service template node.
		let serviceNode = serviceTemplateNode.cloneNode(true);

		// Set a new ID on the node.
		serviceNode.id = this.servicePrefix + serviceDomain;

		// Store the service domain in the document.
		serviceNode.setAttribute('data-service-domain', serviceDomain);
		
		//
		let currentServiceList = JSON.parse(localStorage.services);
		let currentIdentityList = JSON.parse(localStorage.identities);

		//
		let currentService = currentServiceList[serviceDomain];
		
		//
		if(typeof currentService.O !== 'undefined' && currentService.O !== null)
		{
			// Update the template to reflect the services name and url.
			serviceNode.getElementsByClassName('service_name')[0].innerHTML = serviceDomain;
			serviceNode.getElementsByClassName('service_url')[0].innerHTML = currentService.O;
		}
		else
		{
			// Update the template to reflect the services name and url.
			serviceNode.getElementsByClassName('service_name')[0].innerHTML = serviceDomain;
			serviceNode.getElementsByClassName('service_url')[0].innerHTML = currentService.CN;
		}

		// Update the template to reflect the service logo.
		serviceNode.getElementsByClassName('service_logo')[0].src = currentService.F;

		// Update the template to reflect the style of the service.
		serviceNode.getElementsByClassName('service_block_header')[0].setAttribute('style', 'color: ' + currentService.S['color'] + '; background-color: ' + currentService.S['background-color'] + ';');

		//
		let identityList = serviceNode.getElementsByClassName('service_identities')[0];
		let identityNode = identityList.getElementsByTagName('li')[0];

		for(let currentIdentity in currentService.identities)
		{
			// Create a copy of the template identity node.
			let currentIdentityNode = identityNode.cloneNode(true);

			//
			currentIdentityNode.setAttribute('data-identity', currentIdentity);

			// Set a suitable identity name.
			currentIdentityNode.getElementsByClassName('identity_name')[0].innerHTML = this.getIdentityTitle(currentIdentity);

			// Update the clone to reflect the identity name.
			currentIdentityNode.getElementsByClassName('identity_last_used')[0].innerHTML = "Last used " + moment(currentService.identities[currentIdentity].last_used).fromNow();

			// ??
			currentIdentityNode.addEventListener("click", window.cashid.toggleDefaultIdentity);

			if(currentService.defaultIdentity == currentIdentity)
			{
				currentIdentityNode.getElementsByClassName('identity_logo')[0].className = "identity_logo fas fa-bolt";
			}

			// Add the cloned identity to the service.
			identityNode.parentNode.appendChild(currentIdentityNode);
		}

		// Remove the template identity node.
		identityNode.parentNode.removeChild(identityNode);

		// Add the service to the service list.
		serviceTemplateNode.parentNode.appendChild(serviceNode);
	}

	// Hide a service from the user interface.
	this.hideService = function(serviceDomain)
	{
		// Lookup the service node to remove.
		let serviceNode = document.getElementById(this.servicePrefix + serviceDomain);

		// Delete the service node.
		serviceNode.parentNode.removeChild(serviceNode);
	}

	// Get a list of registered services from local storage.
	this.listServices = function()
	{
		
	}

	this.updateMetadataValue = function(field, value)
	{
		// Load the existing settings.
		let currentMetadata = JSON.parse(localStorage.metadata);

		// Apply the new value.
		currentMetadata.profile[field] = value;

		// Store the settings in local storage
		localStorage.metadata = JSON.stringify(currentMetadata);

		// Update the results of the new value.
		this.parseMetadata();
	}

	this.updateMetadataSharing = function(field, bool)
	{
		// Load the existing settings.
		let currentMetadata = JSON.parse(localStorage.metadata);

		// Apply the new value.
		currentMetadata.sharing[field] = bool;

		// Store the settings in local storage
		localStorage.metadata = JSON.stringify(currentMetadata);

		// Update the results of the new value.
		this.parseMetadata();
	}

	//
	this.parseProfile = function(profileJSON)
	{
		//alert(profileJSON);

		// Wrap this in a try statement so we can ignore errors.
		try
		{
			// Parse the contact data.
			let contactProfile = JSON.parse(profileJSON);

			// Load the existing user profile.
			let currentMetadata = JSON.parse(localStorage.metadata);

			// Apply the new value.
			Object.assign(currentMetadata.profile, contactProfile);

			// Use 'Home' email only.
			currentMetadata.profile.email = (contactProfile.email.home ? contactProfile.email.home : null);

			// Use 'Mobile' phonenumber only.
			currentMetadata.profile.phone = (contactProfile.phone.mobile ? contactProfile.phone.mobile : null);

			// Set twitter and facebook separately.
			currentMetadata.profile.twitter = (contactProfile.instant.twitter ? contactProfile.instant.twitter : null);
			currentMetadata.profile.facebook = (contactProfile.instant.facebook ? contactProfile.instant.facebook : null);

			// Store the settings in local storage
			localStorage.metadata = JSON.stringify(currentMetadata);

			// Update the metadata form.
			window.cashid.parseMetadata();
		}
		catch(error)
		{
			alert('Unable to find useful information in the address book.');
		}

		// Show the profile edit dialog.
		$('#metadata').modal('show');
	}

	this.parseMetadata = function()
	{
			// Load the existing settings.
		let currentMetadata = JSON.parse(localStorage.metadata);

		// Idenfification
		document.getElementById("metadata_name").getElementsByClassName("metadata_control")[0].checked =			(currentMetadata.sharing.name ? true : false);
		document.getElementById("metadata_family").getElementsByClassName("metadata_control")[0].checked =			(currentMetadata.sharing.family ? true : false);
		document.getElementById("metadata_nickname").getElementsByClassName("metadata_control")[0].checked =		(currentMetadata.sharing.nickname ? true : false);
		document.getElementById("metadata_age").getElementsByClassName("metadata_control")[0].checked =				(currentMetadata.sharing.age ? true : false);
		document.getElementById("metadata_gender").getElementsByClassName("metadata_control")[0].checked =			(currentMetadata.sharing.gender ? true : false);
		document.getElementById("metadata_birthdate").getElementsByClassName("metadata_control")[0].checked =		(currentMetadata.sharing.birthdate ? true : false);

		document.getElementById("metadata_name").getElementsByClassName("metadata_value")[0].value =					(currentMetadata.profile.name || "");
		document.getElementById("metadata_family").getElementsByClassName("metadata_value")[0].value =				(currentMetadata.profile.family || "");
		document.getElementById("metadata_nickname").getElementsByClassName("metadata_value")[0].value =			(currentMetadata.profile.nickname || "");
		document.getElementById("metadata_age").getElementsByClassName("metadata_value")[0].value =					(currentMetadata.profile.age || "");
		document.getElementById("metadata_gender").getElementsByClassName("metadata_value")[0].value =				(currentMetadata.profile.gender || "");
		document.getElementById("metadata_birthdate").getElementsByClassName("metadata_value")[0].value =			(currentMetadata.profile.birthdate || "");

		// Idenfification
		document.getElementById("metadata_country").getElementsByClassName("metadata_control")[0].checked = 		(currentMetadata.sharing.country ? true : false);
		document.getElementById("metadata_state").getElementsByClassName("metadata_control")[0].checked = 			(currentMetadata.sharing.state ? true : false);
		document.getElementById("metadata_city").getElementsByClassName("metadata_control")[0].checked = 			(currentMetadata.sharing.city ? true : false);
		document.getElementById("metadata_street").getElementsByClassName("metadata_control")[0].checked = 		(currentMetadata.sharing.street ? true : false);
		document.getElementById("metadata_residence").getElementsByClassName("metadata_control")[0].checked = 	(currentMetadata.sharing.residence ? true : false);
		document.getElementById("metadata_coordinate").getElementsByClassName("metadata_control")[0].checked = 	(currentMetadata.sharing.coordinate ? true : false);

		document.getElementById("metadata_country").getElementsByClassName("metadata_value")[0].value = 			(currentMetadata.profile.country || "");
		document.getElementById("metadata_state").getElementsByClassName("metadata_value")[0].value = 				(currentMetadata.profile.state || "");
		document.getElementById("metadata_city").getElementsByClassName("metadata_value")[0].value = 				(currentMetadata.profile.city || "");
		document.getElementById("metadata_street").getElementsByClassName("metadata_value")[0].value = 				(currentMetadata.profile.street || "");
		document.getElementById("metadata_residence").getElementsByClassName("metadata_value")[0].value = 			(currentMetadata.profile.residence || "");
		document.getElementById("metadata_coordinate").getElementsByClassName("metadata_value")[0].value = 		(currentMetadata.profile.coordinate || "");

		// Contact information
		document.getElementById("metadata_email").getElementsByClassName("metadata_control")[0].checked =			(currentMetadata.sharing.email ? true : false);
		document.getElementById("metadata_phone").getElementsByClassName("metadata_control")[0].checked = 			(currentMetadata.sharing.phone ? true : false);
		document.getElementById("metadata_facebook").getElementsByClassName("metadata_control")[0].checked = 		(currentMetadata.sharing.facebook ? true : false);
		document.getElementById("metadata_twitter").getElementsByClassName("metadata_control")[0].checked = 		(currentMetadata.sharing.twitter ? true : false);

		document.getElementById("metadata_email").getElementsByClassName("metadata_value")[0].value = 				(currentMetadata.profile.email ? currentMetadata.profile.email : "");
		document.getElementById("metadata_phone").getElementsByClassName("metadata_value")[0].value = 				(currentMetadata.profile.phone ? currentMetadata.profile.phone : "");
		document.getElementById("metadata_facebook").getElementsByClassName("metadata_value")[0].value = 			(currentMetadata.profile.social ? currentMetadata.profile.facebook : "");
		document.getElementById("metadata_twitter").getElementsByClassName("metadata_value")[0].value = 			(currentMetadata.profile.instant ? currentMetadata.profile.twitter : "");
	}

	this.onScan = function(error, text)
	{
		// Fade back to the service list.
		document.getElementById('ui_main').style.opacity = 1;

		// Hide and destroy the scanning preview/process.
		window.QRScanner.cancelScan();

		if(!error)
		{
			// Vibrate.
			navigator.vibrate(VIBRATE['NOTIFY']);

			if(text.substring(0, 7) === 'cashid:')
			{
				let requestParts = regexps.request.exec(text);

				//alert(requestParts[regnames.request.scheme]);
				//alert(requestParts[regnames.request.domain]);
				//alert(requestParts[regnames.request.path]);
				//alert(requestParts[regnames.request.parameters]);

				window.cashid.identifyToService(requestParts[regnames.request.domain], text);
			}
			else
			{
				alert('Scanned code is not a cashid request.');
			}
		}
	}

	//
	this.toggleScanner = function()
	{
		QRScanner.getStatus
		(
			function(status)
			{
				if(status.scanning)
				{
					// Fade back to the service list.
					document.getElementById('ui_main').style.opacity = 1;

					// Hide and destroy the scanning preview/process.
					window.QRScanner.cancelScan();
				}
				else
				{
					// Start the scan
					window.QRScanner.scan(window.cashid.onScan);

					// Make the webview background transparent;
					window.QRScanner.show();

					// Fade to the scanning preview.
					document.getElementById('ui_main').style.opacity = 0;
				}
			}
		)
	}
}

// Initialize the CashID library.
window.cashid = new CashID();
