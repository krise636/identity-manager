var exec = require('cordova/exec');

exports.getColorStyleFromImageFile = function (arg0, success, error) {
    exec(success, error, 'color_parser', 'getColorStyleFromImageFile', [arg0]);
};
