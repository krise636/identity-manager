var exec = require('cordova/exec');

exports.fetchX509SubjectFromDomain = function(arg0, success, error)
{
	exec(success, error, 'certificate_parser', 'fetchX509SubjectFromDomain', [arg0]);
};
