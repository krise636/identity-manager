package cashid.certificate_parser;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import javax.security.cert.CertificateException;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;

/**
 * This class allows fetching the X.509 subject part 
 * from an SSL certificate by connecting to the domain 
 * to read the certificate
 */
public class certificate_parser extends CordovaPlugin
{
	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException
	{
		if(action.equals("fetchX509SubjectFromDomain"))
		{
				String message = args.getString(0);
				this.fetchX509SubjectFromDomain(message, callbackContext);
				return true;
		}
		return false;
	}

	private void fetchX509SubjectFromDomain(String domain, CallbackContext callbackContext)
	{
		cordova.getThreadPool().execute
		(
			new Runnable()
			{
				public void run()
				{
					try
					{
						// Create a new connection and configure it with the domain requested.
						final HttpsURLConnection connection = (HttpsURLConnection) new URL(domain).openConnection();

						// Set the timeout to two seconds, then connect to the server.
						connection.setConnectTimeout(2000);
						connection.connect();

						// Copy the certificate form the connection.
						final Certificate serverCertificate = connection.getServerCertificates()[0];

						// If the certificate is of the preoper X.509 type..
						if(serverCertificate instanceof X509Certificate)
						{
							// Cast it to an instance of a class that handles X.509 certificates.
							X509Certificate serverCertificateX509 = (X509Certificate) serverCertificate;

							// Return the certificates DN section.
							callbackContext.success(serverCertificateX509.getSubjectDN().getName());
						}
						else
						{
							// If the certificate was in any other form than X.509, report unknown certificate type.
							callbackContext.error("UNKNOWN_CERTIFICATE_TYPE. Details: " + serverCertificate.getType());
						}
					}
					catch (Exception e)
					{
						callbackContext.error("CONNECTION_FAILED. Details: " + e.getMessage());
					}
				}
			}
		);
	}
}
