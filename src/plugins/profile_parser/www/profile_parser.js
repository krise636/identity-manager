var exec = require('cordova/exec');

exports.getOwnerContact = function (success, error) {
    exec(success, error, 'profile_parser', 'getOwnerContact');
};
