package cashid.profile_parser;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.content.pm.PackageManager;

/**
 * This class parses the self contact in the android address book and returns profile information.
 */
public class profile_parser extends CordovaPlugin
{
	public static final String READ_CONTACTS_PERMISSION = android.Manifest.permission.READ_CONTACTS;
	public static final int READ_CONTACTS_REQUEST_CODE = 0;

	private static CallbackContext callbackContext;
	
	@Override
	public boolean execute(String action, JSONArray arguments, CallbackContext callbackContext) throws JSONException
	{
		// Store the callback context.
		this.callbackContext = callbackContext;

		if(!cordova.hasPermission(READ_CONTACTS_PERMISSION))
		{
			cordova.requestPermission(this, READ_CONTACTS_REQUEST_CODE, READ_CONTACTS_PERMISSION);
		}
		else
		{
			this.getOwnerContact();
		}

		return true;
	}

	//
	public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) throws JSONException
	{
		// If the request was denied, return a permission denied message, then abort.
		for(int r:grantResults)
		{
			if(r == PackageManager.PERMISSION_DENIED)
			{
					this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "PERMISSION_DENIED"));
					return;
			}
		}

		// If the request was granted, gather the profile information.
		if(requestCode == READ_CONTACTS_REQUEST_CODE)
		{
			this.getOwnerContact();
		}
	}

	private void getOwnerContact()
	{
		// Create an empty JSON object to hold the profile.
		JSONObject profileJSON = new JSONObject();

		// Create contact objects to store multiple contact types.
		JSONObject profileEmails = new JSONObject();
		JSONObject profileInstantMessengers = new JSONObject();
		JSONObject profileSocialMediaAccounts = new JSONObject();
		JSONObject profilePhoneNumbers = new JSONObject();
		JSONObject profilePostalLabels = new JSONObject();

		// Filter only for birthdays.
		//String selection = ContactsContract.Data.MIMETYPE + "= ? AND " + ContactsContract.CommonDataKinds.Event.TYPE + "=" + ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY;

		String[] projection =
		{
			ContactsContract.Data.MIMETYPE,
			ContactsContract.Data.DATA1,
			ContactsContract.Data.DATA2,
			ContactsContract.Data.DATA3,
			ContactsContract.Data.DATA4,
			ContactsContract.Data.DATA5,
			ContactsContract.Data.DATA6,
			ContactsContract.Data.DATA7,
			ContactsContract.Data.DATA8,
			ContactsContract.Data.DATA9,
			ContactsContract.Data.DATA10,
			ContactsContract.Data.DATA11,
			ContactsContract.Data.DATA12,
			ContactsContract.Data.DATA13,
			ContactsContract.Data.DATA14,
			ContactsContract.Data.DATA15
		};

		// Request the owner profile and instantiate a cursor to manage the database result.
		Cursor cProfile = cordova.getActivity().getContentResolver().query(Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI, ContactsContract.Contacts.Data.CONTENT_DIRECTORY), projection, null, null, null);

		if(cProfile.getCount() >= 1)
		{
			while(cProfile.moveToNext())
			{
				// Get column names for 
				String[] profileColumns = cProfile.getColumnNames();

				try
				{
					// One switch per metadata category to be able to match the same case more than once.
					// This is because android stores some data that is used in more than one context.

					// i: identification
					switch(cProfile.getString(0))
					{
						// i1: name, i2: family
						case ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE:
						{
							profileJSON.put("name", cProfile.getString(2));
							profileJSON.put("family", cProfile.getString(3));
							break;
						}

						// i3: nickname
						case ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE:
						{
							profileJSON.put("nickname", cProfile.getString(1));
							break;
						}

						// i4: age
						// i5: gender
						// i6: birthdate
						// i7:

						// i8: picture
						case ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE:
						{
							profileJSON.put("picture_file", cProfile.getString(14));
							break;
						}

						// i9: national
					}

					// p: position
					switch(cProfile.getString(0))
					{
						// p1: country
						// p2: state
						// p3: city
						// p4: street name / route
						case ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE:
						{
							profileJSON.put("country", cProfile.getString(10));
							profileJSON.put("state", cProfile.getString(8));
							profileJSON.put("city", cProfile.getString(7));
							profileJSON.put("street", cProfile.getString(4));
							break;
						}
						// p5: street number
						// p6: residence
						// p7: 
						// p8: 
						// p9: coordinate
					}

					// c: contact
					switch(cProfile.getString(0))
					{
						// c1: email 
						case ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE:
						{
							int emailType = Integer.parseInt(cProfile.getString(2));

							if(emailType == ContactsContract.CommonDataKinds.BaseTypes.TYPE_CUSTOM)
							{
								profileEmails.put(cProfile.getString(3), cProfile.getString(1));
							}
							else if(emailType == ContactsContract.CommonDataKinds.Email.TYPE_HOME)
							{
								profileEmails.put("home", cProfile.getString(1));
							}
							else if(emailType == ContactsContract.CommonDataKinds.Email.TYPE_WORK)
							{
								profileEmails.put("work", cProfile.getString(1));
							}
							else if(emailType == ContactsContract.CommonDataKinds.Email.TYPE_MOBILE)
							{
								profileEmails.put("mobile", cProfile.getString(1));
							}
							else if(emailType == ContactsContract.CommonDataKinds.Email.TYPE_OTHER)
							{
								profileEmails.put("other", cProfile.getString(1));
							}

							break;
						}

						// c2: instant
						case ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE:
						{
							int imProtocol = Integer.parseInt(cProfile.getString(5));
							String imProtocols[] = 
							{
								"aim", 
								"msn",
								"yahoo",
								"skype",
								"qq",
								"googletalk",
								"icq",
								"jabber",
								"netmeeting"
							};

							if(imProtocol == ContactsContract.CommonDataKinds.Im.PROTOCOL_CUSTOM)
							{
								profileInstantMessengers.put(cProfile.getString(6), cProfile.getString(1));
							}
							else
							{
								profileInstantMessengers.put(imProtocols[imProtocol - 1], cProfile.getString(1));
							}

							break;
						}

						// c3: social / website
						case ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE:
						{
							int webType = Integer.parseInt(cProfile.getString(2));
							String webTypes[] = 
							{
								"homepage", 
								"blog",
								"profile",
								"home",
								"work",
								"ftp",
								"other"
							};

							if(webType == ContactsContract.CommonDataKinds.BaseTypes.TYPE_CUSTOM)
							{
								profileSocialMediaAccounts.put(cProfile.getString(3), cProfile.getString(1));
							}
							else
							{
								profileSocialMediaAccounts.put(webTypes[webType - 1], cProfile.getString(1));
							}

							break;
						}

						// c4: phone
						case ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE:
						{
							int phoneType = Integer.parseInt(cProfile.getString(2));
							String phoneTypes[] = 
							{
								"home", 
								"mobile",
								"work",
								"faxwork",
								"faxhome",
								"pager",
								"other",
								"callback",
								"car",
								"company",
								"isdn",
								"main",
								"faxother",
								"radio",
								"telex",
								"ttyttd",
								"workmobile",
								"workpager",
								"assistant",
								"mms"
							};

							if(phoneType == ContactsContract.CommonDataKinds.BaseTypes.TYPE_CUSTOM)
							{
								profilePhoneNumbers.put(cProfile.getString(3), cProfile.getString(1));
							}
							else
							{
								profilePhoneNumbers.put(phoneTypes[phoneType - 1], cProfile.getString(1));
							}

							break;
						}

						// c5: postal
						case ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE:
						{
							int postalType = Integer.parseInt(cProfile.getString(2));
							String postalTypes[] = 
							{
								"home", 
								"work",
								"other"
							};

							if(postalType == ContactsContract.CommonDataKinds.BaseTypes.TYPE_CUSTOM)
							{
								profilePostalLabels.put(cProfile.getString(3), cProfile.getString(1));
							}
							else
							{
								profilePostalLabels.put(postalTypes[postalType - 1], cProfile.getString(1));
							}

							break;
						}

						// c6: 
						// c7: 
						// c8: 
						// c9: 
					}

					profileJSON.put("email", profileEmails);
					profileJSON.put("instant", profileInstantMessengers);
					profileJSON.put("social", profileSocialMediaAccounts);
					profileJSON.put("phone", profilePhoneNumbers);
					profileJSON.put("postal", profilePostalLabels);
				}
				catch(JSONException e)
				{
					// Create a new result set.
					PluginResult result = new PluginResult(PluginResult.Status.OK, "Could not add profile data to json object:" + e);

					// Send the result to the webview.
					this.callbackContext.sendPluginResult(result); 

					//
					return ;
				}
			}

			// Close the cursor.
			cProfile.close();

			// Create a new result set.
			PluginResult result = new PluginResult(PluginResult.Status.OK, profileJSON.toString());

			// Send the result to the webview.
			this.callbackContext.sendPluginResult(result); 
		}
		else
		{
			// Create a new result set.
			PluginResult result = new PluginResult(PluginResult.Status.OK, "Could not find an owner contact in the addressbook.");

			// Send the result to the webview.
			this.callbackContext.sendPluginResult(result); 
		}
	}
}
